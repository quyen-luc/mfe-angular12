import { Component, EventEmitter, Input, Output, VERSION } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @Input() message: string;
  @Output() clicked = new EventEmitter<string>();

  ngVersion = VERSION.full
}
